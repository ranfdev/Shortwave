# Russian translation for shortwave.
# Copyright (C) 2021 shortwave's COPYRIGHT HOLDER
# This file is distributed under the same license as the shortwave package.
# Alexey Rubtsov <rushills@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: shortwave master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Shortwave/issues\n"
"POT-Creation-Date: 2021-11-04 09:51+0000\n"
"PO-Revision-Date: 2021-12-29 11:02+0200\n"
"Last-Translator: Alexey Rubtsov <rushills@gmail.com>\n"
"Language-Team: Russian <gnome-cyr@gnome.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 3.0.1\n"

#: data/gtk/discover_page.ui:28
msgid "Most voted stations"
msgstr "Самые популярные станции"

#: data/gtk/discover_page.ui:43
msgid "Trending"
msgstr "В тренде"

#: data/gtk/discover_page.ui:58
msgid "Other users are listening to…"
msgstr "Другие пользователи слушают…"

#: data/gtk/library_page.ui:60
msgid "Receiving station data…"
msgstr "Получение данных о станции…"

#: data/gtk/library_page.ui:87
msgid "_Discover new stations"
msgstr "Откройте для себя _новые станции"

#: data/gtk/mini_controller.ui:87
msgid "SHORTWAVE INTERNET RADIO"
msgstr "КОРОТКОВОЛНОВОЕ ИНТЕРНЕТ-РАДИО"

#: data/gtk/mini_controller.ui:109 data/gtk/sidebar_controller.ui:63
#: data/gtk/toolbar_controller.ui:38
msgid "No playback"
msgstr "Нет воспроизведения"

#: data/gtk/notification.ui:58
msgid "Error details"
msgstr "Детали ошибки"

#: data/gtk/player.ui:44
msgid "Connected with"
msgstr "Подключено к"

#: data/gtk/settings_window.ui:7 data/gtk/help_overlay.ui:11
msgid "General"
msgstr "Общие"

#: data/gtk/settings_window.ui:10
msgid "Appearance"
msgstr "Вид"

#: data/gtk/settings_window.ui:14
msgid "_Dark Mode"
msgstr "Темный режим"

#: data/gtk/settings_window.ui:16
msgid "Whether the application should use a dark theme"
msgstr "Должно ли приложение использовать темную тему"

#: data/gtk/settings_window.ui:29
msgid "Features"
msgstr "Возможности"

#: data/gtk/settings_window.ui:32
msgid "_Notifications"
msgstr "Включить _уведомления"

#: data/gtk/settings_window.ui:34
msgid "Show desktop notifications when a new song gets played"
msgstr ""
"Показывать уведомления на рабочем столе при воспроизведении новой песни"

#: data/gtk/help_overlay.ui:15
msgctxt "shortcut window"
msgid "Open shortcut window"
msgstr "Открыть окно быстрого доступа"

#: data/gtk/help_overlay.ui:21
msgctxt "shortcut window"
msgid "Open application menu"
msgstr "Открыть меню приложения"

#: data/gtk/help_overlay.ui:27
msgctxt "shortcut window"
msgid "Open preferences"
msgstr "Открыть настройки"

#: data/gtk/help_overlay.ui:33
msgctxt "shortcut window"
msgid "Quit the application"
msgstr "Закрыть приложение"

#: data/gtk/help_overlay.ui:40
msgid "Window"
msgstr "Окно"

#: data/gtk/help_overlay.ui:44
msgctxt "shortcut window"
msgid "Open library view"
msgstr "Открыть библиотеку"

#: data/gtk/help_overlay.ui:50
msgctxt "shortcut window"
msgid "Open discover view"
msgstr "Открыть режим исследования"

#: data/gtk/help_overlay.ui:56
msgctxt "shortcut window"
msgid "Open search view"
msgstr "Открыть поиск"

#: data/gtk/help_overlay.ui:62
msgctxt "shortcut window"
msgid "Go back"
msgstr "Назад"

#: data/gtk/help_overlay.ui:69
msgid "Playback"
msgstr "Воспроизведение"

#: data/gtk/help_overlay.ui:73
msgctxt "shortcut window"
msgid "Toggle playback"
msgstr "Переключение воспроизведения"

#: data/gtk/search_page.ui:31 data/gtk/station_dialog.ui:172
#: src/ui/pages/search_page.rs:183
msgid "Votes"
msgstr "Голоса"

#: data/gtk/search_page.ui:36
msgid "Change the sorting of the search results"
msgstr "Изменить сортировку результатов поиска"

#: data/gtk/search_page.ui:63
msgid "No results"
msgstr "Нет результатов"

#: data/gtk/search_page.ui:64
msgid "Try using a different search term"
msgstr "Попробуйте использовать другие термины"

#: data/gtk/search_page.ui:148 data/gtk/window.ui:106
msgid "_Name"
msgstr "_Название"

#: data/gtk/search_page.ui:153 data/gtk/window.ui:111
msgid "_Language"
msgstr "_Язык"

#: data/gtk/search_page.ui:158 data/gtk/window.ui:116
msgid "_Country"
msgstr "С_трана"

#: data/gtk/search_page.ui:163 data/gtk/window.ui:121
msgid "S_tate"
msgstr "_Штат"

#: data/gtk/search_page.ui:168 data/gtk/window.ui:126
msgid "_Votes"
msgstr "_Голоса"

#: data/gtk/search_page.ui:173 data/gtk/window.ui:131
msgid "_Bitrate"
msgstr "_Битрейт"

#: data/gtk/search_page.ui:180 data/gtk/window.ui:138
msgid "_Ascending"
msgstr "_По возрастанию"

#: data/gtk/search_page.ui:185 data/gtk/window.ui:143
msgid "_Descending"
msgstr "По _убыванию"

#: data/gtk/sidebar_controller.ui:22
msgid "An error occurred"
msgstr "Произошла ошибка"

#: data/gtk/sidebar_controller.ui:188
msgid "_Show station details"
msgstr "_Показать информацию о станции"

#: data/gtk/sidebar_controller.ui:192
msgid "Stream to a _device"
msgstr "Вывести на _устройство"

#: data/gtk/sidebar_controller.ui:198
msgid "_Enable mini player"
msgstr "_Включить мини-плеер"

#: data/gtk/song_listbox.ui:23
msgid ""
"Songs are automatically recognized using the stream metadata.\n"
"\n"
"If the station does not send any metadata, no songs can be recognized."
msgstr ""
"Песни автоматически распознаются с помощью метаданных потока.\n"
"\n"
"Если станция не отправляет метаданные, песни не распознаются."

#: data/gtk/song_listbox.ui:66
msgid "No songs detected"
msgstr "Песни не обнаружены"

#: data/gtk/song_listbox.ui:76
msgid "Detected songs will appear here."
msgstr "Обнаруженные композиции появятся здесь."

#: data/gtk/song_listbox.ui:85
msgid "_How does this work?"
msgstr "_Как это работает?"

#: data/gtk/song_listbox.ui:124
msgid "Saved songs are located in your Music folder."
msgstr "Сохраненные композиции находятся в папке Музыка."

#: data/gtk/song_listbox.ui:137
msgid "_Open"
msgstr "_Открыть"

#: data/gtk/song_row.ui:14
msgid "Save recorded song"
msgstr "Сохранить записанную песню"

#: data/gtk/station_dialog.ui:8
msgid "Station details"
msgstr "Информация о станции"

#: data/gtk/station_dialog.ui:87
msgid "_Play this station"
msgstr "_Воспроизвести эту станцию"

#: data/gtk/station_dialog.ui:100
msgid "_Add to library"
msgstr "Добавить в _библиотеку"

#: data/gtk/station_dialog.ui:114
msgid "_Remove from library"
msgstr "_Удалить из библиотеки"

#: data/gtk/station_dialog.ui:128
msgid "Information"
msgstr "Информация"

#: data/gtk/station_dialog.ui:131
msgid "This is a local private station."
msgstr "Это локальная частная станция."

#: data/gtk/station_dialog.ui:132
msgid ""
"This station is visible only to you. If you remove this station from your "
"library, you have to create it again."
msgstr ""
"Эта станция видна только вам. Если вы удалите эту станцию, её придётся "
"создавать заново."

#: data/gtk/station_dialog.ui:138 src/ui/pages/search_page.rs:180
msgid "Language"
msgstr "Язык"

#: data/gtk/station_dialog.ui:152
msgid "Tags"
msgstr "Теги"

#: data/gtk/station_dialog.ui:187
msgid "Location"
msgstr "Местоположение"

#: data/gtk/station_dialog.ui:192 src/ui/pages/search_page.rs:181
msgid "Country"
msgstr "Страна"

#: data/gtk/station_dialog.ui:206 src/ui/pages/search_page.rs:182
msgid "State"
msgstr "Штат"

#: data/gtk/station_dialog.ui:244
msgid "Audio"
msgstr "Аудио"

#: data/gtk/station_dialog.ui:248 src/ui/pages/search_page.rs:184
msgid "Bitrate"
msgstr "Битрейт"

#: data/gtk/station_dialog.ui:262
msgid "Codec"
msgstr "Кодек"

#. This is a noun/label for the station stream url
#: data/gtk/station_dialog.ui:276
msgid "Stream"
msgstr "Поток"

#: data/gtk/streaming_dialog.ui:47
msgid "Stream to a device"
msgstr "Потоковая передача на устройство"

#: data/gtk/streaming_dialog.ui:52
msgid "_Cancel"
msgstr "_Отмена"

#: data/gtk/streaming_dialog.ui:58
msgid "_Connect"
msgstr "_Подключиться"

#: data/gtk/streaming_dialog.ui:90
msgid "Devices which implement the Google Cast protocol are supported."
msgstr "Поддерживаются устройства, поддерживающие протокол Google Cast."

#: data/gtk/streaming_dialog.ui:122
msgid "No devices found"
msgstr "Устройства не найдены"

#: data/gtk/streaming_dialog.ui:132
msgid "_Retry search"
msgstr "По_вторить поиск"

#: data/gtk/streaming_dialog.ui:180 data/gtk/streaming_dialog.ui:211
msgid "Searching for devices…"
msgstr "Поиск устройств…"

#: data/gtk/window.ui:103
msgid "_Sorting"
msgstr "_Сортировка"

#: data/gtk/window.ui:152 data/gtk/window.ui:180
msgid "_Open radio-browser.info <sup>↗</sup>"
msgstr "_Открыть radio-browser.info <sup>↗</sup>"

#: data/gtk/window.ui:157 data/gtk/window.ui:185
msgid "_Create new station <sup>↗</sup>"
msgstr "Создать _новую станцию <sup>↗</sup>"

#: data/gtk/window.ui:164 data/gtk/window.ui:192
msgid "_Preferences"
msgstr "_Параметры"

#: data/gtk/window.ui:168 data/gtk/window.ui:196
msgid "_Keyboard Shortcuts"
msgstr "_Горячие клавиши"

#: data/gtk/window.ui:172 data/gtk/window.ui:200
msgid "_About Shortwave"
msgstr "О Коротковолновом Интернет-Радио"

#: src/audio/player.rs:168
msgid "This station cannot be played because the stream is offline."
msgstr ""
"Эта станция не может быть воспроизведена, поскольку поток находится в "
"автономном режиме."

#: src/audio/player.rs:186
msgid "Station cannot be streamed."
msgstr "Станция не может быть передана в потоковом режиме."

#: src/audio/player.rs:186
msgid "URL is not valid."
msgstr "URL не действителен."

#: src/database/library.rs:271
msgid "An invalid station was removed from the library."
msgstr "Из библиотеки была удалена недействительная станция."

#: src/ui/about_dialog.rs:32
#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:6
msgid "Listen to internet radio"
msgstr "Слушайте интернет-радио"

#: src/ui/about_dialog.rs:35
msgid "translator-credits"
msgstr ""
"Launchpad Contributions:\n"
"  Aleksey Kabanov https://launchpad.net/~ak099"

#. TODO: Implement show-server-stats action
#: src/ui/pages/discover_page.rs:91
msgid "Show statistics"
msgstr "Показать статистику"

#: src/ui/pages/discover_page.rs:92
msgid "Browse over 25,500 stations"
msgstr "Просмотр более 25 500 станций"

#: src/ui/pages/discover_page.rs:94
msgid "Add new station"
msgstr "Добавить новую станцию"

#: src/ui/pages/discover_page.rs:95
msgid "Your favorite station is missing?"
msgstr "Ваша любимая станция пропала?"

#: src/ui/pages/discover_page.rs:97
msgid "Open website"
msgstr "Открыть веб-сайт"

#: src/ui/pages/discover_page.rs:98
msgid "Powered by radio-browser.info"
msgstr "Работает на сайте radio-browser.info"

#: src/ui/pages/discover_page.rs:137 src/ui/pages/search_page.rs:269
msgid "Station data could not be received."
msgstr "Данные станции не могут быть получены."

#. Welcome text which gets displayed when the library is empty. "{}" is the application name.
#: src/ui/pages/library_page.rs:117
msgid "Welcome to {}"
msgstr "Добро пожаловать на {}"

#: src/ui/pages/search_page.rs:106
msgid ""
"The number of results is limited to {} item. Try using a more specific "
"search term."
msgid_plural ""
"The number of results is limited to {} items. Try using a more specific "
"search term."
msgstr[0] "Количество результатов ограничено до {}. Попробуйте уточнить поиск."
msgstr[1] "Количество результатов ограничено до {}. Попробуйте уточнить поиск."
msgstr[2] "Количество результатов ограничено до {}. Попробуйте уточнить поиск."

#: src/ui/pages/search_page.rs:179
msgid "Name"
msgstr "Название"

#: src/ui/station_dialog.rs:276
msgid "{} kbit/s"
msgstr "{} кБит/с"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:5
#: data/de.haeckerfelix.Shortwave.desktop.in.in:3
msgid "Shortwave"
msgstr "Коротковолновое Интернет-Радио"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:8
msgid "Felix Häcker"
msgstr "Феликс Хеккер (Felix Häcker)"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:16
msgid ""
"Shortwave is an internet radio player that provides access to a station "
"database with over 25,000 stations."
msgstr ""
"Коротковолновое Интернет-Радио - это интернет-радиоплеер, который "
"предоставляет доступ к базе данных станций, насчитывающей более 25 000 "
"станций."

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:19
msgid "Features:"
msgstr "Возможности:"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:21
msgid "Create your own library where you can add your favorite stations"
msgstr ""
"Создайте собственную библиотеку, в которую можно добавлять любимые станции"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:22
msgid "Easily search and discover new radio stations"
msgstr "Легкий поиск и открытие новых радиостанций"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:23
msgid ""
"Automatic recognition of songs, with the possibility to save them "
"individually"
msgstr ""
"Автоматическое распознавание песен с возможностью их индивидуального "
"сохранения"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:24
msgid "Responsive application layout, compatible for small and large screens"
msgstr ""
"Отзывчивый интерфейс приложения, совместимый с маленькими и большими экранами"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:25
msgid "Play audio on supported network devices (e.g. Google Chromecasts)"
msgstr ""
"Воспроизведение аудио на поддерживаемых сетевых устройствах (например, "
"Google Chromecasts)"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:26
msgid "Seamless integration into the GNOME desktop environment"
msgstr "Бесшовная интеграция в среду рабочего стола GNOME"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/de.haeckerfelix.Shortwave.desktop.in.in:12
msgid "Gradio;Radio;Stream;Wave;"
msgstr "Gradio;Радио;Стриминг;Волна;"

#~ msgid "Homepage"
#~ msgstr "Главная страница"

#~ msgid "{} Vote"
#~ msgid_plural "{} Votes"
#~ msgstr[0] "{} Голос"
#~ msgstr[1] "{} Голоса"
#~ msgstr[2] "{} Голосов"

#~ msgid "{} · {} Vote"
#~ msgid_plural "{} · {} Votes"
#~ msgstr[0] "{} - {} Голос"
#~ msgstr[1] "{} - {} Голоса"
#~ msgstr[2] "{} - {} Голосов"
